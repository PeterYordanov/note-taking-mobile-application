package com.example.androidnotesapplication.Database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class NotepadDatabaseSingleton_Impl extends NotepadDatabaseSingleton {
  private volatile NoteDataAccessObject _noteDataAccessObject;

  private volatile TaskDataAccessObject _taskDataAccessObject;

  private volatile DiaryDataAccessObject _diaryDataAccessObject;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `notes` (`noteId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `text` TEXT, `date` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `tasks` (`taskId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `dateReminder` INTEGER NOT NULL, `hourReminder` INTEGER NOT NULL, `minuteReminder` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `diaries` (`diaryId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `image` BLOB, `text` TEXT, `date` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"0a679bdc9b20c3ecf51122cd1df23a1b\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `notes`");
        _db.execSQL("DROP TABLE IF EXISTS `tasks`");
        _db.execSQL("DROP TABLE IF EXISTS `diaries`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsNotes = new HashMap<String, TableInfo.Column>(3);
        _columnsNotes.put("noteId", new TableInfo.Column("noteId", "INTEGER", true, 1));
        _columnsNotes.put("text", new TableInfo.Column("text", "TEXT", false, 0));
        _columnsNotes.put("date", new TableInfo.Column("date", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNotes = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNotes = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNotes = new TableInfo("notes", _columnsNotes, _foreignKeysNotes, _indicesNotes);
        final TableInfo _existingNotes = TableInfo.read(_db, "notes");
        if (! _infoNotes.equals(_existingNotes)) {
          throw new IllegalStateException("Migration didn't properly handle notes(com.example.androidnotesapplication.Models.Note).\n"
                  + " Expected:\n" + _infoNotes + "\n"
                  + " Found:\n" + _existingNotes);
        }
        final HashMap<String, TableInfo.Column> _columnsTasks = new HashMap<String, TableInfo.Column>(5);
        _columnsTasks.put("taskId", new TableInfo.Column("taskId", "INTEGER", true, 1));
        _columnsTasks.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsTasks.put("dateReminder", new TableInfo.Column("dateReminder", "INTEGER", true, 0));
        _columnsTasks.put("hourReminder", new TableInfo.Column("hourReminder", "INTEGER", true, 0));
        _columnsTasks.put("minuteReminder", new TableInfo.Column("minuteReminder", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTasks = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTasks = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTasks = new TableInfo("tasks", _columnsTasks, _foreignKeysTasks, _indicesTasks);
        final TableInfo _existingTasks = TableInfo.read(_db, "tasks");
        if (! _infoTasks.equals(_existingTasks)) {
          throw new IllegalStateException("Migration didn't properly handle tasks(com.example.androidnotesapplication.Models.Task).\n"
                  + " Expected:\n" + _infoTasks + "\n"
                  + " Found:\n" + _existingTasks);
        }
        final HashMap<String, TableInfo.Column> _columnsDiaries = new HashMap<String, TableInfo.Column>(4);
        _columnsDiaries.put("diaryId", new TableInfo.Column("diaryId", "INTEGER", true, 1));
        _columnsDiaries.put("image", new TableInfo.Column("image", "BLOB", false, 0));
        _columnsDiaries.put("text", new TableInfo.Column("text", "TEXT", false, 0));
        _columnsDiaries.put("date", new TableInfo.Column("date", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDiaries = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesDiaries = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoDiaries = new TableInfo("diaries", _columnsDiaries, _foreignKeysDiaries, _indicesDiaries);
        final TableInfo _existingDiaries = TableInfo.read(_db, "diaries");
        if (! _infoDiaries.equals(_existingDiaries)) {
          throw new IllegalStateException("Migration didn't properly handle diaries(com.example.androidnotesapplication.Models.Diary).\n"
                  + " Expected:\n" + _infoDiaries + "\n"
                  + " Found:\n" + _existingDiaries);
        }
      }
    }, "0a679bdc9b20c3ecf51122cd1df23a1b", "94dd7c79d4a10dae23c427b75a1d992c");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "notes","tasks","diaries");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `notes`");
      _db.execSQL("DELETE FROM `tasks`");
      _db.execSQL("DELETE FROM `diaries`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public NoteDataAccessObject notesDAO() {
    if (_noteDataAccessObject != null) {
      return _noteDataAccessObject;
    } else {
      synchronized(this) {
        if(_noteDataAccessObject == null) {
          _noteDataAccessObject = new NoteDataAccessObject_Impl(this);
        }
        return _noteDataAccessObject;
      }
    }
  }

  @Override
  public TaskDataAccessObject tasksDAO() {
    if (_taskDataAccessObject != null) {
      return _taskDataAccessObject;
    } else {
      synchronized(this) {
        if(_taskDataAccessObject == null) {
          _taskDataAccessObject = new TaskDataAccessObject_Impl(this);
        }
        return _taskDataAccessObject;
      }
    }
  }

  @Override
  public DiaryDataAccessObject diariesDAO() {
    if (_diaryDataAccessObject != null) {
      return _diaryDataAccessObject;
    } else {
      synchronized(this) {
        if(_diaryDataAccessObject == null) {
          _diaryDataAccessObject = new DiaryDataAccessObject_Impl(this);
        }
        return _diaryDataAccessObject;
      }
    }
  }
}
