package com.example.androidnotesapplication.Database;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.androidnotesapplication.Models.Diary;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class DiaryDataAccessObject_Impl implements DiaryDataAccessObject {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfDiary;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfDiary;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfDiary;

  private final SharedSQLiteStatement __preparedStmtOfDeleteDiaryById;

  private final SharedSQLiteStatement __preparedStmtOfDeleteDiaryByText;

  public DiaryDataAccessObject_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDiary = new EntityInsertionAdapter<Diary>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `diaries`(`diaryId`,`image`,`text`,`date`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Diary value) {
        stmt.bindLong(1, value.getDiaryId());
        if (value.getImage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindBlob(2, value.getImage());
        }
        if (value.getText() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getText());
        }
        stmt.bindLong(4, value.getDate());
      }
    };
    this.__deletionAdapterOfDiary = new EntityDeletionOrUpdateAdapter<Diary>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `diaries` WHERE `diaryId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Diary value) {
        stmt.bindLong(1, value.getDiaryId());
      }
    };
    this.__updateAdapterOfDiary = new EntityDeletionOrUpdateAdapter<Diary>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `diaries` SET `diaryId` = ?,`image` = ?,`text` = ?,`date` = ? WHERE `diaryId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Diary value) {
        stmt.bindLong(1, value.getDiaryId());
        if (value.getImage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindBlob(2, value.getImage());
        }
        if (value.getText() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getText());
        }
        stmt.bindLong(4, value.getDate());
        stmt.bindLong(5, value.getDiaryId());
      }
    };
    this.__preparedStmtOfDeleteDiaryById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM diaries WHERE diaryId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteDiaryByText = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM diaries WHERE text = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertDiary(Diary diary) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfDiary.insert(diary);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteDiary(Diary diary) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfDiary.handle(diary);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateDiary(Diary diary) {
    __db.beginTransaction();
    try {
      __updateAdapterOfDiary.handle(diary);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteDiaryById(int diaryId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteDiaryById.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, diaryId);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteDiaryById.release(_stmt);
    }
  }

  @Override
  public void deleteDiaryByText(String diaryText) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteDiaryByText.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (diaryText == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, diaryText);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteDiaryByText.release(_stmt);
    }
  }

  @Override
  public List<Diary> getDiaries() {
    final String _sql = "SELECT * FROM diaries";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfDiaryId = _cursor.getColumnIndexOrThrow("diaryId");
      final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
      final List<Diary> _result = new ArrayList<Diary>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Diary _item;
        final byte[] _tmpImage;
        _tmpImage = _cursor.getBlob(_cursorIndexOfImage);
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDate;
        _tmpDate = _cursor.getLong(_cursorIndexOfDate);
        _item = new Diary(_tmpImage,_tmpText,_tmpDate);
        final int _tmpDiaryId;
        _tmpDiaryId = _cursor.getInt(_cursorIndexOfDiaryId);
        _item.setDiaryId(_tmpDiaryId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Diary getDiaryById(int diaryId) {
    final String _sql = "SELECT * FROM diaries WHERE diaryId = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, diaryId);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfDiaryId = _cursor.getColumnIndexOrThrow("diaryId");
      final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
      final Diary _result;
      if(_cursor.moveToFirst()) {
        final byte[] _tmpImage;
        _tmpImage = _cursor.getBlob(_cursorIndexOfImage);
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDate;
        _tmpDate = _cursor.getLong(_cursorIndexOfDate);
        _result = new Diary(_tmpImage,_tmpText,_tmpDate);
        final int _tmpDiaryId;
        _tmpDiaryId = _cursor.getInt(_cursorIndexOfDiaryId);
        _result.setDiaryId(_tmpDiaryId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Diary getDiaryByText(String diaryText) {
    final String _sql = "SELECT * FROM diaries WHERE text = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (diaryText == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, diaryText);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfDiaryId = _cursor.getColumnIndexOrThrow("diaryId");
      final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
      final Diary _result;
      if(_cursor.moveToFirst()) {
        final byte[] _tmpImage;
        _tmpImage = _cursor.getBlob(_cursorIndexOfImage);
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDate;
        _tmpDate = _cursor.getLong(_cursorIndexOfDate);
        _result = new Diary(_tmpImage,_tmpText,_tmpDate);
        final int _tmpDiaryId;
        _tmpDiaryId = _cursor.getInt(_cursorIndexOfDiaryId);
        _result.setDiaryId(_tmpDiaryId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
