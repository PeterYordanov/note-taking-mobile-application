package com.example.androidnotesapplication.Database;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.androidnotesapplication.Models.Task;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class TaskDataAccessObject_Impl implements TaskDataAccessObject {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfTask;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfTask;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfTask;

  private final SharedSQLiteStatement __preparedStmtOfDeleteTaskById;

  private final SharedSQLiteStatement __preparedStmtOfDeleteTaskByText;

  public TaskDataAccessObject_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfTask = new EntityInsertionAdapter<Task>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `tasks`(`taskId`,`title`,`dateReminder`,`hourReminder`,`minuteReminder`) VALUES (nullif(?, 0),?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Task value) {
        stmt.bindLong(1, value.getTaskId());
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        stmt.bindLong(3, value.getDateReminder());
        stmt.bindLong(4, value.getHourReminder());
        stmt.bindLong(5, value.getMinuteReminder());
      }
    };
    this.__deletionAdapterOfTask = new EntityDeletionOrUpdateAdapter<Task>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `tasks` WHERE `taskId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Task value) {
        stmt.bindLong(1, value.getTaskId());
      }
    };
    this.__updateAdapterOfTask = new EntityDeletionOrUpdateAdapter<Task>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `tasks` SET `taskId` = ?,`title` = ?,`dateReminder` = ?,`hourReminder` = ?,`minuteReminder` = ? WHERE `taskId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Task value) {
        stmt.bindLong(1, value.getTaskId());
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        stmt.bindLong(3, value.getDateReminder());
        stmt.bindLong(4, value.getHourReminder());
        stmt.bindLong(5, value.getMinuteReminder());
        stmt.bindLong(6, value.getTaskId());
      }
    };
    this.__preparedStmtOfDeleteTaskById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM tasks WHERE taskId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteTaskByText = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM tasks WHERE title = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertTask(Task task) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfTask.insert(task);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteTask(Task task) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfTask.handle(task);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateTask(Task task) {
    __db.beginTransaction();
    try {
      __updateAdapterOfTask.handle(task);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteTaskById(int taskId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteTaskById.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, taskId);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteTaskById.release(_stmt);
    }
  }

  @Override
  public void deleteTaskByText(String title) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteTaskByText.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (title == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, title);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteTaskByText.release(_stmt);
    }
  }

  @Override
  public List<Task> getTasks() {
    final String _sql = "SELECT * FROM tasks";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfDateReminder = _cursor.getColumnIndexOrThrow("dateReminder");
      final int _cursorIndexOfHourReminder = _cursor.getColumnIndexOrThrow("hourReminder");
      final int _cursorIndexOfMinuteReminder = _cursor.getColumnIndexOrThrow("minuteReminder");
      final List<Task> _result = new ArrayList<Task>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Task _item;
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        final long _tmpDateReminder;
        _tmpDateReminder = _cursor.getLong(_cursorIndexOfDateReminder);
        final int _tmpHourReminder;
        _tmpHourReminder = _cursor.getInt(_cursorIndexOfHourReminder);
        final int _tmpMinuteReminder;
        _tmpMinuteReminder = _cursor.getInt(_cursorIndexOfMinuteReminder);
        _item = new Task(_tmpTitle,_tmpDateReminder,_tmpHourReminder,_tmpMinuteReminder);
        final int _tmpTaskId;
        _tmpTaskId = _cursor.getInt(_cursorIndexOfTaskId);
        _item.setTaskId(_tmpTaskId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Task getTaskById(int taskId) {
    final String _sql = "SELECT * FROM tasks WHERE taskId = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, taskId);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfDateReminder = _cursor.getColumnIndexOrThrow("dateReminder");
      final int _cursorIndexOfHourReminder = _cursor.getColumnIndexOrThrow("hourReminder");
      final int _cursorIndexOfMinuteReminder = _cursor.getColumnIndexOrThrow("minuteReminder");
      final Task _result;
      if(_cursor.moveToFirst()) {
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        final long _tmpDateReminder;
        _tmpDateReminder = _cursor.getLong(_cursorIndexOfDateReminder);
        final int _tmpHourReminder;
        _tmpHourReminder = _cursor.getInt(_cursorIndexOfHourReminder);
        final int _tmpMinuteReminder;
        _tmpMinuteReminder = _cursor.getInt(_cursorIndexOfMinuteReminder);
        _result = new Task(_tmpTitle,_tmpDateReminder,_tmpHourReminder,_tmpMinuteReminder);
        final int _tmpTaskId;
        _tmpTaskId = _cursor.getInt(_cursorIndexOfTaskId);
        _result.setTaskId(_tmpTaskId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Task getTaskByText(String title) {
    final String _sql = "SELECT * FROM tasks WHERE title = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (title == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, title);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfDateReminder = _cursor.getColumnIndexOrThrow("dateReminder");
      final int _cursorIndexOfHourReminder = _cursor.getColumnIndexOrThrow("hourReminder");
      final int _cursorIndexOfMinuteReminder = _cursor.getColumnIndexOrThrow("minuteReminder");
      final Task _result;
      if(_cursor.moveToFirst()) {
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        final long _tmpDateReminder;
        _tmpDateReminder = _cursor.getLong(_cursorIndexOfDateReminder);
        final int _tmpHourReminder;
        _tmpHourReminder = _cursor.getInt(_cursorIndexOfHourReminder);
        final int _tmpMinuteReminder;
        _tmpMinuteReminder = _cursor.getInt(_cursorIndexOfMinuteReminder);
        _result = new Task(_tmpTitle,_tmpDateReminder,_tmpHourReminder,_tmpMinuteReminder);
        final int _tmpTaskId;
        _tmpTaskId = _cursor.getInt(_cursorIndexOfTaskId);
        _result.setTaskId(_tmpTaskId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
