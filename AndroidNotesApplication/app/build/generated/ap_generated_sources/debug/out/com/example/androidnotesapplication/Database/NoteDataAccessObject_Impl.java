package com.example.androidnotesapplication.Database;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.androidnotesapplication.Models.Note;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class NoteDataAccessObject_Impl implements NoteDataAccessObject {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfNote;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfNote;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfNote;

  private final SharedSQLiteStatement __preparedStmtOfDeleteNoteById;

  private final SharedSQLiteStatement __preparedStmtOfDeleteNoteByText;

  public NoteDataAccessObject_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfNote = new EntityInsertionAdapter<Note>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `notes`(`noteId`,`text`,`date`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Note value) {
        stmt.bindLong(1, value.getNoteId());
        if (value.getText() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getText());
        }
        stmt.bindLong(3, value.getDateModified());
      }
    };
    this.__deletionAdapterOfNote = new EntityDeletionOrUpdateAdapter<Note>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `notes` WHERE `noteId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Note value) {
        stmt.bindLong(1, value.getNoteId());
      }
    };
    this.__updateAdapterOfNote = new EntityDeletionOrUpdateAdapter<Note>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `notes` SET `noteId` = ?,`text` = ?,`date` = ? WHERE `noteId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Note value) {
        stmt.bindLong(1, value.getNoteId());
        if (value.getText() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getText());
        }
        stmt.bindLong(3, value.getDateModified());
        stmt.bindLong(4, value.getNoteId());
      }
    };
    this.__preparedStmtOfDeleteNoteById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM notes WHERE noteId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteNoteByText = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM notes WHERE text = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertNote(Note note) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfNote.insert(note);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteNote(Note note) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfNote.handle(note);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateNote(Note note) {
    __db.beginTransaction();
    try {
      __updateAdapterOfNote.handle(note);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteNoteById(int noteId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteNoteById.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, noteId);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteNoteById.release(_stmt);
    }
  }

  @Override
  public void deleteNoteByText(String text) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteNoteByText.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (text == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, text);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteNoteByText.release(_stmt);
    }
  }

  @Override
  public List<Note> getNotes() {
    final String _sql = "SELECT * FROM notes";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfNoteId = _cursor.getColumnIndexOrThrow("noteId");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDateModified = _cursor.getColumnIndexOrThrow("date");
      final List<Note> _result = new ArrayList<Note>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Note _item;
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDateModified;
        _tmpDateModified = _cursor.getLong(_cursorIndexOfDateModified);
        _item = new Note(_tmpText,_tmpDateModified);
        final int _tmpNoteId;
        _tmpNoteId = _cursor.getInt(_cursorIndexOfNoteId);
        _item.setNoteId(_tmpNoteId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Note getNoteById(int noteId) {
    final String _sql = "SELECT * FROM notes WHERE noteId = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, noteId);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfNoteId = _cursor.getColumnIndexOrThrow("noteId");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDateModified = _cursor.getColumnIndexOrThrow("date");
      final Note _result;
      if(_cursor.moveToFirst()) {
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDateModified;
        _tmpDateModified = _cursor.getLong(_cursorIndexOfDateModified);
        _result = new Note(_tmpText,_tmpDateModified);
        final int _tmpNoteId;
        _tmpNoteId = _cursor.getInt(_cursorIndexOfNoteId);
        _result.setNoteId(_tmpNoteId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Note getNoteByText(String text) {
    final String _sql = "SELECT * FROM notes WHERE text = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (text == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, text);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfNoteId = _cursor.getColumnIndexOrThrow("noteId");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfDateModified = _cursor.getColumnIndexOrThrow("date");
      final Note _result;
      if(_cursor.moveToFirst()) {
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        final long _tmpDateModified;
        _tmpDateModified = _cursor.getLong(_cursorIndexOfDateModified);
        _result = new Note(_tmpText,_tmpDateModified);
        final int _tmpNoteId;
        _tmpNoteId = _cursor.getInt(_cursorIndexOfNoteId);
        _result.setNoteId(_tmpNoteId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
