package com.example.androidnotesapplication.StateMachine;

import java.util.List;

public interface IState<T> {

    void    load(List<T> list);
    List<T> search(String text);
    void    hideWidgets();
    void    showWidgets();
    void    removeByText(String text);
    String  getStateType();
    List<T> getDatabaseItems();
    List<T> getDataSourceItems();

}
