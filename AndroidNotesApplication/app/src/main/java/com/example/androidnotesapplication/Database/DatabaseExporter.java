package com.example.androidnotesapplication.Database;

import android.content.Context;
import android.widget.Toast;

import com.ajts.androidmads.sqliteimpex.SQLiteImporterExporter;

public class DatabaseExporter {

    private SQLiteImporterExporter importerExporter;

    public DatabaseExporter(final Context context, String tag) {
        importerExporter = new SQLiteImporterExporter(context, tag);
        importerExporter.setOnExportListener(new SQLiteImporterExporter.ExportListener() {
            @Override
            public void onSuccess(String message) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Exception exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void export(String path)
    {
        try {
            importerExporter.exportDataBase(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
