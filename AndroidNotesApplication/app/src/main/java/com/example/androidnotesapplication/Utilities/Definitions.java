package com.example.androidnotesapplication.Utilities;

public class Definitions {
    public static final int    REQUEST_IMAGE_CAPTURE = 0x1 << 1;
    public static final int    TITLE_DELETE_MESSAGE_LIMIT = 25;
    public static final String DIARY_EXTRA_KEY = "diary_id";
    public static final String NOTE_EXTRA_KEY = "note_id";
    public static final String TASK_EXTRA_KEY = "task_id";
    public static final String OPEN_IMAGE_EXTRA_KEY = "img_id";
    public static final String DATABASE_NAME = "notepadDb";
}
