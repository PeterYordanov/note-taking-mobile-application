package com.example.androidnotesapplication.StateMachine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidnotesapplication.Adapters.DiariesAdapter;
import com.example.androidnotesapplication.ModifyDiaryActivity;
import com.example.androidnotesapplication.Database.DiaryDataAccessObject;
import com.example.androidnotesapplication.Models.Diary;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiaryState extends AppCompatActivity implements IState<Diary> {

    private ArrayList<Diary> diaries;
    private DiariesAdapter diariesAdapter;

    private DiaryDataAccessObject diariesDataAccessObject;
    private Context context;

    private ListView listView;
    private FloatingActionButton addDiaryButton;

    public DiaryState(Context context) {
        this.context = context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        addDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, ModifyDiaryActivity.class);
                startActivity(in);
            }
        });

    }

    @Override
    public void load(List<Diary> diaries) {

        Collections.reverse(diaries);

        this.diaries = new ArrayList<>();
        this.diaries.addAll(diaries);
        this.diariesAdapter = new DiariesAdapter(context, this.diaries);
        listView.setAdapter(this.diariesAdapter);

    }

    @Override
    public List<Diary> getDataSourceItems() {
        return diaries;
    }

    @Override
    public List<Diary> search(String text) {

        ArrayList<Diary> filteredDiaries = new ArrayList<>();
        List<Diary> allDiaries = diariesDataAccessObject.getDiaries();

        for(Diary item : allDiaries) {
            if(item.getText().toLowerCase().contains(text.toLowerCase())) {
                filteredDiaries.add(item);
            }
        }

        return filteredDiaries;

    }

    @Override
    public void hideWidgets() {

        listView.setVisibility(View.INVISIBLE);
        addDiaryButton.setVisibility(View.INVISIBLE);

    }

    @Override
    public void showWidgets() {

        listView.setClickable(true);
        listView.setVisibility(View.VISIBLE);
        addDiaryButton.setVisibility(View.VISIBLE);

    }

    @Override
    public void removeByText(String text) {
        diariesDataAccessObject.deleteDiaryByText(text);
    }

    //Setters
    public void setDiariesDataAccessObject(DiaryDataAccessObject diariesDataAccessObject) {
        this.diariesDataAccessObject = diariesDataAccessObject;

    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void setAddDiaryButton(FloatingActionButton addDiaryButton) {
        this.addDiaryButton = addDiaryButton;
    }


    //Getters

    public ListView getListView() {
        return listView;
    }

    public FloatingActionButton getAddDiaryButton() {
        return addDiaryButton;
    }

    public String getStateType() {
        return "Diaries";
    }

    public List<Diary> getDatabaseItems() {
        return diariesDataAccessObject.getDiaries();
    }

}
