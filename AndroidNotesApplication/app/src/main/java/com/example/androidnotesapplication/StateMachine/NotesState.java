package com.example.androidnotesapplication.StateMachine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidnotesapplication.Adapters.NotesAdapter;
import com.example.androidnotesapplication.ModifyNoteActivity;
import com.example.androidnotesapplication.Database.NoteDataAccessObject;
import com.example.androidnotesapplication.Models.Note;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotesState extends AppCompatActivity implements IState<Note> {

    private ArrayList<Note> notes;
    private NotesAdapter notesAdapter;

    private NoteDataAccessObject notesDataAccessObject;
    private Context context;

    private ListView listView;
    private FloatingActionButton addNoteButton;

    public NotesState(Context context) {
        this.context = context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ModifyNoteActivity.class));
            }
        });

    }

    @Override
    public void load(List<Note> notes) {

        Collections.reverse(notes);

        this.notes = new ArrayList<>();
        this.notes.addAll(notes);
        this.notesAdapter = new NotesAdapter(context, this.notes);
        listView.setAdapter(this.notesAdapter);

    }

    @Override
    public List<Note> getDataSourceItems()
    {
        return notes;
    }


    @Override
    public List<Note> search(String text) {

        ArrayList<Note> filteredNotes = new ArrayList<>();
        List<Note> allNotes = notesDataAccessObject.getNotes();

        for(Note item : allNotes) {
            if(item.getText().toLowerCase().contains(text.toLowerCase())) {
                filteredNotes.add(item);
            }
        }

        return filteredNotes;

    }

    @Override
    public void hideWidgets() {

        listView.setVisibility(View.INVISIBLE);
        addNoteButton.setVisibility(View.INVISIBLE);

    }

    @Override
    public void showWidgets() {

        listView.setClickable(true);
        addNoteButton.setVisibility(View.VISIBLE);
        listView.setVisibility(View.VISIBLE);

    }

    @Override
    public void removeByText(String text) {
        notesDataAccessObject.deleteNoteByText(text);
    }

    //Setters
    public void setNotesDataAccessObject(NoteDataAccessObject notesDataAccessObject) {
        this.notesDataAccessObject = notesDataAccessObject;
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void setAddNoteButton(FloatingActionButton addNoteButton) {
        this.addNoteButton = addNoteButton;
    }

    //Getters

    public ListView getListView() {
        return listView;
    }

    public FloatingActionButton getAddNoteButton() {
        return addNoteButton;
    }

    public String getStateType() {
        return "Notes";
    }

    public List<Note> getDatabaseItems() {
        return notesDataAccessObject.getNotes();
    }

}
