package com.example.androidnotesapplication.Models;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "tasks")
public class Task {
    @PrimaryKey(autoGenerate = true)
    private int taskId;

    public Task(String title, long dateReminder, int hourReminder, int minuteReminder) {
        this.title = title;
        this.dateReminder = dateReminder;
        this.hourReminder = hourReminder;
        this.minuteReminder = minuteReminder;
    }

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "dateReminder")
    private long dateReminder;
    @ColumnInfo(name = "hourReminder")
    private int hourReminder;
    @ColumnInfo(name = "minuteReminder")
    private int minuteReminder;

    @Ignore
    public Task(){}


    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDateReminder() {
        return dateReminder;
    }

    public void setDateReminder(long dateReminder) {
        this.dateReminder = dateReminder;
    }

    public int getHourReminder() {
        return hourReminder;
    }

    public void setHourReminder(int hourReminder) {
        this.hourReminder = hourReminder;
    }

    public int getMinuteReminder() {
        return minuteReminder;
    }

    public void setMinuteReminder(int minuteReminder) {
        this.minuteReminder = minuteReminder;
    }
}

