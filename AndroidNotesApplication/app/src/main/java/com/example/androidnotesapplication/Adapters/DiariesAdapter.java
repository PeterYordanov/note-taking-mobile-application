package com.example.androidnotesapplication.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.androidnotesapplication.Models.Diary;
import com.example.androidnotesapplication.R;
import com.example.androidnotesapplication.Utilities.General;

import java.util.List;

public class DiariesAdapter extends ArrayAdapter<Diary> {

    public DiariesAdapter(Context context, List<Diary> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Diary diary = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.diary_layout, parent, false);
        }

        ImageView imageView = convertView.findViewById(R.id.diary_layout_image);
        TextView dateView = convertView.findViewById(R.id.diary_layout_date);
        TextView textView = convertView.findViewById(R.id.diary_layout_text);

        imageView.setImageBitmap(General.getBitmapFromByteArray(diary.getImage()));
        dateView.setText(General.getStringDateFromLong(diary.getDate()));
        textView.setText(diary.getText());

        return convertView;
    }
}