package com.example.androidnotesapplication.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidnotesapplication.Models.Task;
import com.example.androidnotesapplication.R;
import com.example.androidnotesapplication.Utilities.General;

import java.util.List;

public class TasksAdapter extends ArrayAdapter<Task> {

    public TasksAdapter(Context context, List<Task> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Task task = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.task_layout, parent, false);
        }

        TextView title = convertView.findViewById(R.id.task_layout_text);
        TextView dateTime = convertView.findViewById(R.id.task_layout_date);

        title.setText(task.getTitle());

        String date = General.getStringDateFromLong(task.getDateReminder());
        dateTime.setText("Reminder: " + date.substring(0, date.length() - 8) + task.getHourReminder() + ":" + task.getMinuteReminder());

        return convertView;
    }
}