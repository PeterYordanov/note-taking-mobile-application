package com.example.androidnotesapplication.Models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "notes")
public class Note {
    @PrimaryKey(autoGenerate = true)
    private int noteId;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "date")
    private long dateModified;

    public Note(String text, long dateModified) {
        this.text = text;
        this.dateModified = dateModified;
    }

    @Ignore
    public Note(){}

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int id) {
        this.noteId = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getDateModified() {
        return dateModified;
    }

    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }
}
