package com.example.androidnotesapplication.StateMachine;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidnotesapplication.Adapters.TasksAdapter;
import com.example.androidnotesapplication.Database.TaskDataAccessObject;
import com.example.androidnotesapplication.Models.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TasksState extends AppCompatActivity implements IState<Task> {

    private ArrayList<Task> tasks;
    private TasksAdapter tasksAdapter;

    private TaskDataAccessObject tasksDataAccessObject;
    private Context context;

    private ListView listView;
    private FloatingActionButton addTaskButton;

    public TasksState(Context context) {
        this.context = context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void load(List<Task> tasks) {

        Collections.sort(tasks, new Comparator<Task>(){
            @Override
            public int compare(Task lhs, Task rhs) {
                Date date1 = new Date(rhs.getDateReminder());
                Date date2 = new Date(lhs.getDateReminder());
                return date1.compareTo(date2);
            }
        });

        this.tasks = new ArrayList<>();
        this.tasks.addAll(tasks);
        this.tasksAdapter = new TasksAdapter(context, this.tasks);
        listView.setAdapter(this.tasksAdapter);

    }

    @Override
    public void removeByText(String text) {
        tasksDataAccessObject.deleteTaskByText(text);
    }

    @Override
    public List<Task> search(String text) {

        ArrayList<Task> filteredTasks = new ArrayList<>();
        List<Task> allTasks = tasksDataAccessObject.getTasks();

        for(Task item : allTasks) {
            if(item.getTitle().toLowerCase().contains(text.toLowerCase())) {
                filteredTasks.add(item);
            }
        }

        return filteredTasks;

    }

    @Override
    public void hideWidgets() {

        listView.setVisibility(View.INVISIBLE);
        addTaskButton.setVisibility(View.INVISIBLE);

    }

    @Override
    public void showWidgets() {

        listView.setClickable(true);
        addTaskButton.setVisibility(View.VISIBLE);
        listView.setVisibility(View.VISIBLE);

    }

    //Setters
    public void setTasksDataAccessObject(TaskDataAccessObject tasksDataAccessObject) {
        this.tasksDataAccessObject = tasksDataAccessObject;
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void setAddTaskButton(FloatingActionButton addTaskButton) {
        this.addTaskButton = addTaskButton;
    }


    //Getters

    public ListView getListView() {
        return listView;
    }

    public FloatingActionButton getAddTaskButton() {
        return addTaskButton;
    }

    public String getStateType() {
        return "Tasks";
    }

    public List<Task> getDatabaseItems() {
        return tasksDataAccessObject.getTasks();
    }

    @Override
    public List<Task> getDataSourceItems() {
        return tasks;
    }
}
