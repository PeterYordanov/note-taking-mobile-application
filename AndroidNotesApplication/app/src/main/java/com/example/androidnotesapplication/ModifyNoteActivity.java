package com.example.androidnotesapplication;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidnotesapplication.Database.NotepadDatabaseSingleton;
import com.example.androidnotesapplication.Models.Note;
import androidx.appcompat.app.AppCompatActivity;
import com.example.androidnotesapplication.Database.NoteDataAccessObject;
import com.example.androidnotesapplication.Utilities.General;
import java.util.Date;

import static com.example.androidnotesapplication.Utilities.Definitions.NOTE_EXTRA_KEY;

public class ModifyNoteActivity extends AppCompatActivity {

    private EditText noteText;
    private NoteDataAccessObject noteDataAccessObject;
    private Note temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_add_note);
        noteText = findViewById(R.id.input_note);

        TextView textView = findViewById(R.id.date_time_now);
        textView.setText("Now: " + General.getStringDateFromLong(new Date().getTime()));

        noteText = findViewById(R.id.input_note);
        noteDataAccessObject = NotepadDatabaseSingleton.getInstance(this).notesDAO();

        boolean extrasHaveBeenAdded = getIntent().getExtras() != null;

        if (extrasHaveBeenAdded) {
            String text = getIntent().getExtras().getString(NOTE_EXTRA_KEY, "");

            temp = noteDataAccessObject.getNoteByText(text);

            noteText.setText(temp.getText());
        } else noteText.setFocusable(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        boolean isSaveClicked = item.getItemId() == R.id.save;

        if (isSaveClicked) {
            String text = noteText.getText().toString();
            if (!text.isEmpty()) {
                long date = new Date().getTime();
                if (temp == null) {
                    temp = new Note(text, date);
                    noteDataAccessObject.insertNote(temp);
                    Toast.makeText(getApplicationContext(), "Note added!", Toast.LENGTH_SHORT).show();
                } else {
                    temp.setText(text);
                    temp.setDateModified(date);
                    noteDataAccessObject.updateNote(temp);
                    Toast.makeText(getApplicationContext(), "Note updated!", Toast.LENGTH_SHORT).show();
                }

                finish();
            } else {
                Toast.makeText(getApplicationContext(), "You cannot have an empty note!", Toast.LENGTH_LONG).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }

}
