package com.example.androidnotesapplication.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.androidnotesapplication.Models.Note;

import java.util.List;

@Dao
public interface NoteDataAccessObject {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNote(Note note);
    @Delete
    void deleteNote(Note note);
    @Update
    void updateNote(Note note);
    @Query("SELECT * FROM notes")
    List<Note> getNotes();
    @Query("SELECT * FROM notes WHERE noteId = :noteId")
    Note getNoteById(int noteId);
    @Query("DELETE FROM notes WHERE noteId = :noteId")
    void deleteNoteById(int noteId);
    @Query("DELETE FROM notes WHERE text = :text")
    void deleteNoteByText(String text);
    @Query("SELECT * FROM notes WHERE text = :text")
    Note getNoteByText(String text);

}
