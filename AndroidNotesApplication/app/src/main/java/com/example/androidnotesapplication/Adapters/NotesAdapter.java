package com.example.androidnotesapplication.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidnotesapplication.R;
import com.example.androidnotesapplication.Models.Note;
import com.example.androidnotesapplication.Utilities.General;

import java.util.ArrayList;

public class NotesAdapter extends ArrayAdapter<Note> {

    public NotesAdapter(Context context, ArrayList<Note> notes) {
        super(context, 0, notes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.note_layout, parent, false);
        }

        TextView text = convertView.findViewById(R.id.note_text);
        TextView dateModified = convertView.findViewById(R.id.date_modified);

        text.setText(note.getText());
        dateModified.setText("Last modified: " + General.getStringDateFromLong(note.getDateModified()));

        return convertView;
    }
}
