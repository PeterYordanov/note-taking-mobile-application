package com.example.androidnotesapplication;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidnotesapplication.Database.NotepadDatabaseSingleton;
import com.example.androidnotesapplication.Models.Diary;
import com.example.androidnotesapplication.Utilities.General;

import static com.example.androidnotesapplication.Utilities.Definitions.OPEN_IMAGE_EXTRA_KEY;

public class OpenImageActivity extends AppCompatActivity {

    ImageView imageView;
    Diary temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle("");

        setContentView(R.layout.activity_open_image);

        imageView = findViewById(R.id.open_image);

        if (getIntent().getExtras() != null) {
            String text = getIntent().getExtras().getString(OPEN_IMAGE_EXTRA_KEY, "");

            temp = NotepadDatabaseSingleton.getInstance(this).diariesDAO().getDiaryByText(text);

            imageView.setImageBitmap(General.getBitmapFromByteArray(temp.getImage()));
        }
    }

}
