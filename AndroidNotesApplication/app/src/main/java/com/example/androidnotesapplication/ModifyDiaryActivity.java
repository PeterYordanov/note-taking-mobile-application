package com.example.androidnotesapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.example.androidnotesapplication.Database.DiaryDataAccessObject;
import com.example.androidnotesapplication.Database.NotepadDatabaseSingleton;
import com.example.androidnotesapplication.Models.Diary;
import com.example.androidnotesapplication.Utilities.Definitions;
import com.example.androidnotesapplication.Utilities.General;

import java.io.File;
import java.util.Date;

import static com.example.androidnotesapplication.Utilities.Definitions.DIARY_EXTRA_KEY;
import static com.example.androidnotesapplication.Utilities.Definitions.REQUEST_IMAGE_CAPTURE;

public class ModifyDiaryActivity extends AppCompatActivity {

    private DiaryDataAccessObject diaryDataAccessObject;
    private Diary temp;
    private ImageView diaryImage;
    private EditText diaryText;
    private Button uploadPhotoButton;
    private Intent cameraIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diary);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        diaryImage = findViewById(R.id.diary_image_view);
        diaryText = findViewById(R.id.diary_text);
        uploadPhotoButton = findViewById(R.id.upload_photo);

        uploadPhotoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cameraIntent.resolveActivity(getPackageManager()) != null) {
                        File imageFile = General.getImageFile(ModifyDiaryActivity.this);

                        boolean imageExists = imageFile != null;
                        if(imageExists) {
                            Uri imageUri = FileProvider.getUriForFile(ModifyDiaryActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", imageFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    }
                }
            }
        );

        diaryDataAccessObject = NotepadDatabaseSingleton.getInstance(this).diariesDAO();

        if (getIntent().getExtras() != null) {
            String text = getIntent().getExtras().getString(DIARY_EXTRA_KEY, "");

            temp = diaryDataAccessObject.getDiaryByText(text);

            diaryImage.setImageBitmap(General.getBitmapFromByteArray(temp.getImage()));
            diaryText.setText(temp.getText());
            diaryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ModifyDiaryActivity.this, OpenImageActivity.class);
                    intent.putExtra(Definitions.OPEN_IMAGE_EXTRA_KEY, temp.getText());
                    startActivity(intent);
                }
            });

        } else diaryText.setFocusable(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.save) {
            String text = diaryText.getText().toString();
            byte[] imageByteArray = General.getByteArrayFromBitmap(((BitmapDrawable)diaryImage.getDrawable()).getBitmap());

            if (!text.isEmpty()) {
                long date = new Date().getTime();
                if (temp == null) {
                    temp = new Diary(imageByteArray, text, date);
                    diaryDataAccessObject.insertDiary(temp);
                    Toast.makeText(getApplicationContext(), "Diary added!", Toast.LENGTH_SHORT).show();
                } else {
                    temp.setText(text);
                    temp.setDate(date);
                    temp.setImage(imageByteArray);
                    diaryDataAccessObject.updateDiary(temp);
                    Toast.makeText(getApplicationContext(), "Diary updated!", Toast.LENGTH_SHORT).show();
                }

                finish();
            } else {
                Toast.makeText(getApplicationContext(), "You need to provide diary text or image!", Toast.LENGTH_LONG).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = cameraIntent.getExtras();
            Uri imageUri = (Uri)extras.get(MediaStore.EXTRA_OUTPUT);

            Bitmap bitmap = General.getBitmapFromImageUri(imageUri, this);

            diaryImage.setImageBitmap(bitmap);
        }
    }
}

