package com.example.androidnotesapplication.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.androidnotesapplication.Models.Diary;
import com.example.androidnotesapplication.Models.Note;
import com.example.androidnotesapplication.Models.Task;

import static com.example.androidnotesapplication.Utilities.Definitions.DATABASE_NAME;

@Database(entities = { Note.class, Task.class, Diary.class }, version = 1)
public abstract class NotepadDatabaseSingleton extends RoomDatabase {

    public abstract NoteDataAccessObject notesDAO();
    public abstract TaskDataAccessObject tasksDAO();
    public abstract DiaryDataAccessObject diariesDAO();

    private static NotepadDatabaseSingleton instance;

    public static NotepadDatabaseSingleton getInstance(Context context) {

        if (instance == null) {
            instance = Room.databaseBuilder(context, NotepadDatabaseSingleton.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }

        return instance;
    }
}
