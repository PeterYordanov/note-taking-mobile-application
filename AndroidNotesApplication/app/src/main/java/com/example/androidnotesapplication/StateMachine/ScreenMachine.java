package com.example.androidnotesapplication.StateMachine;

import java.util.List;

public class ScreenMachine {

    private IState currentState;

    public ScreenMachine(IState state) {
        currentState = state;
    }
    public void setState(IState state) {
        currentState = state;
    }
    public void load(List<?> list) { currentState.load(list); }
    public void search(String text) {
        currentState.search(text);
    }
    public void hideWidgets() {
        currentState.hideWidgets();
    }
    public void showWidgets() {
        currentState.showWidgets();
    }
    public IState getState() {
        return currentState;
    }
    public String getStateType() {
        return currentState.getStateType();
    }
    public List<?> getDatabaseItems() {
        return currentState.getDatabaseItems();
    }
    public List<?> getDataSourceItems() { return currentState.getDataSourceItems(); }

}
