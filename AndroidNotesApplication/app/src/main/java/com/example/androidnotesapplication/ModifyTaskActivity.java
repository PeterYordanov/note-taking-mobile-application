package com.example.androidnotesapplication;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidnotesapplication.Database.NotepadDatabaseSingleton;
import com.example.androidnotesapplication.Database.TaskDataAccessObject;
import com.example.androidnotesapplication.Models.Task;
import com.example.androidnotesapplication.Utilities.General;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.example.androidnotesapplication.Utilities.Definitions.TASK_EXTRA_KEY;

public class ModifyTaskActivity extends AppCompatActivity {

    private EditText title;

    private Button inputDateReminderButton;
    private Button inputTimeReminderButton;

    private DatePicker inputDatePicker;
    private TimePicker inputTimePicker;

    private Task temp;
    private TaskDataAccessObject taskDataAccessObject;

    int currentYear = 0;
    int currentMonth = 0;
    int currentDay = 0;

    int currentHour = 0;
    int currentMinute = 0;

    private void updateTime() {
        Calendar calendar = Calendar.getInstance();

        currentYear = calendar.get(Calendar.YEAR);
        currentMonth = calendar.get(Calendar.MONTH);
        currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        currentMinute = calendar.get(Calendar.MINUTE);
        currentHour = calendar.get(Calendar.HOUR);
    }

    private void updateTimeReminderText() {
        TextView textView = findViewById(R.id.textViewTimeReminder);
        textView.setText("Time reminder: " + inputTimePicker.getHour() + ":" + inputTimePicker.getMinute());
    }

    private void updateDateReminderText() {
        TextView textView = findViewById(R.id.textViewDateReminder);
        textView.setText("Date reminder: " + inputDatePicker.getYear() + "." + (inputDatePicker.getMonth() + 1) + "." + inputDatePicker.getDayOfMonth());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        taskDataAccessObject = NotepadDatabaseSingleton.getInstance(this).tasksDAO();

        title = findViewById(R.id.task_title);

        inputDateReminderButton = findViewById(R.id.task_date_picker_button);
        inputTimeReminderButton = findViewById(R.id.task_time_picker_button);

        inputDatePicker = findViewById(R.id.task_date_picker);
        inputTimePicker = findViewById(R.id.task_time_picker);

        updateTime();

        inputDatePicker.updateDate(currentYear, currentMonth, currentDay);
        inputTimePicker.setHour(currentHour);
        inputTimePicker.setMinute(currentMinute);

        inputTimePicker.setIs24HourView(true);

        inputDateReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.add_task_layout).setVisibility(View.INVISIBLE);
                DatePicker datePicker = findViewById(R.id.task_date_picker);
                datePicker.setVisibility(View.VISIBLE);
            }
        });

        inputTimeReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.add_task_layout).setVisibility(View.INVISIBLE);
                TimePicker timePicker = findViewById(R.id.task_time_picker);
                timePicker.setVisibility(View.VISIBLE);
            }
        });

        inputDatePicker.setOnClickListener(new DatePicker.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputDatePicker.setVisibility(View.INVISIBLE);
                findViewById(R.id.add_task_layout).setVisibility(View.VISIBLE);

                inputDatePicker.setMinDate(System.currentTimeMillis() - 1000);

                updateDateReminderText();
            }
        });

        inputTimePicker.setOnClickListener(new TimePicker.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputTimePicker.setVisibility(View.INVISIBLE);
                findViewById(R.id.add_task_layout).setVisibility(View.VISIBLE);

                updateTimeReminderText();
            }
        });

        boolean extrasHaveBeenAdded = getIntent().getExtras() != null;

        if (extrasHaveBeenAdded) {

            String text = getIntent().getExtras().getString(TASK_EXTRA_KEY, "");

            temp = taskDataAccessObject.getTaskByText(text);

            title.setText(temp.getTitle());

            Date tempDate = new Date(temp.getDateReminder());
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
            cal.setTime(tempDate);

            inputDatePicker.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            inputTimePicker.setHour(temp.getHourReminder());
            inputTimePicker.setMinute(temp.getMinuteReminder());

            updateDateReminderText();
            updateTimeReminderText();

        } else title.setFocusable(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        boolean isSaveClicked = item.getItemId() == R.id.save;

        if (isSaveClicked) {

            String text = title.getText().toString();
            DatePicker date = findViewById(R.id.task_date_picker);
            TimePicker time = findViewById(R.id.task_time_picker);

            Date d = new Date(General.getDateFromDatePicker(date).getTime());

            if (!text.isEmpty()) {
                if (temp == null) {
                    temp = new Task(text, d.getTime(), time.getHour(), time.getMinute());
                    taskDataAccessObject.insertTask(temp);
                    Toast.makeText(getApplicationContext(), "Task added!", Toast.LENGTH_SHORT).show();
                } else {
                    temp.setTitle(text);
                    temp.setDateReminder(General.getStringDateFromDatetime(d));
                    temp.setHourReminder(time.getHour());
                    temp.setMinuteReminder(time.getMinute());
                    taskDataAccessObject.updateTask(temp);
                    Toast.makeText(getApplicationContext(), "Task updated!", Toast.LENGTH_SHORT).show();
                }

                finish();
            } else {
                Toast.makeText(getApplicationContext(), "You cannot have an empty task title!", Toast.LENGTH_LONG).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}