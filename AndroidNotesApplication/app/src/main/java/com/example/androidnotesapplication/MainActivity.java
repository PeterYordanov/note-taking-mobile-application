package com.example.androidnotesapplication;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.androidnotesapplication.Database.NotepadDatabaseSingleton;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.example.androidnotesapplication.Models.Diary;
import com.example.androidnotesapplication.Models.Task;
import com.example.androidnotesapplication.Models.Note;

import com.example.androidnotesapplication.StateMachine.DiaryState;
import com.example.androidnotesapplication.StateMachine.NotesState;
import com.example.androidnotesapplication.StateMachine.TasksState;
import com.example.androidnotesapplication.StateMachine.ScreenMachine;

import java.util.List;

import static com.example.androidnotesapplication.Utilities.Definitions.DIARY_EXTRA_KEY;
import static com.example.androidnotesapplication.Utilities.Definitions.NOTE_EXTRA_KEY;
import static com.example.androidnotesapplication.Utilities.Definitions.TASK_EXTRA_KEY;
import static com.example.androidnotesapplication.Utilities.Definitions.TITLE_DELETE_MESSAGE_LIMIT;

public class MainActivity extends AppCompatActivity {

    ScreenMachine stateMachine;
    TasksState tasksState;
    NotesState notesState;
    DiaryState diaryState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tasksState = new TasksState(this);
        diaryState = new DiaryState(this);
        notesState = new NotesState(this);

        tasksState.setListView((ListView)findViewById(R.id.tasks_list_view));
        diaryState.setListView((ListView)findViewById(R.id.diaries_list_view));
        notesState.setListView((ListView)findViewById(R.id.notes_list_view));

        tasksState.setAddTaskButton((FloatingActionButton) findViewById(R.id.add_task));
        diaryState.setAddDiaryButton((FloatingActionButton) findViewById(R.id.add_diary));
        notesState.setAddNoteButton((FloatingActionButton) findViewById(R.id.add_note));

        tasksState.setTasksDataAccessObject(NotepadDatabaseSingleton.getInstance(this).tasksDAO());
        diaryState.setDiariesDataAccessObject(NotepadDatabaseSingleton.getInstance(this).diariesDAO());
        notesState.setNotesDataAccessObject(NotepadDatabaseSingleton.getInstance(this).notesDAO());

        notesState.load(notesState.getDatabaseItems());
        tasksState.load(tasksState.getDatabaseItems());
        diaryState.load(diaryState.getDatabaseItems());

        setNoteOnTap();

        tasksState.getAddTaskButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, ModifyTaskActivity.class);
                startActivity(in);
            }

        });

        diaryState.getAddDiaryButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, ModifyDiaryActivity.class);
                startActivity(in);
            }

        });

        notesState.getAddNoteButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, ModifyNoteActivity.class);
                startActivity(in);
            }

        });

        notesState.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, final View arg1, final int pos, long id) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Delete Note '" + getTitleDeleteMessageFromTextView((TextView) arg1.findViewById(R.id.note_text)) + "' ?")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                TextView textView = arg1.findViewById(R.id.note_text);
                                notesState.removeByText(textView.getText().toString());
                                notesState.load(notesState.getDatabaseItems());
                                updateActionBarTitle();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create().show();
                return true;
            }
        });

        tasksState.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, final View arg1, final int pos, long id) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Delete Task '" + getTitleDeleteMessageFromTextView((TextView) arg1.findViewById(R.id.task_layout_text)) + "' ?")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                TextView textView = arg1.findViewById(R.id.task_layout_text);
                                tasksState.removeByText(textView.getText().toString());
                                tasksState.load(tasksState.getDatabaseItems());
                                updateActionBarTitle();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create().show();
                return true;
            }
        });

        diaryState.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, final View arg1, final int pos, long id) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Delete Diary '" + getTitleDeleteMessageFromTextView((TextView) arg1.findViewById(R.id.diary_layout_text)) + "' ?")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                TextView textView = arg1.findViewById(R.id.diary_layout_text);
                                diaryState.removeByText(textView.getText().toString());
                                diaryState.load(diaryState.getDatabaseItems());
                                updateActionBarTitle();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create().show();
                return true;
            }
        });

        stateMachine = new ScreenMachine(notesState);
        stateMachine.showWidgets();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_notes:

                        stateMachine.setState(tasksState);
                        stateMachine.hideWidgets();
                        stateMachine.setState(diaryState);
                        stateMachine.hideWidgets();

                        stateMachine.setState(notesState);

                        setNoteOnTap();
                        break;

                    case R.id.navigation_tasks:

                        stateMachine.setState(diaryState);
                        stateMachine.hideWidgets();
                        stateMachine.setState(notesState);
                        stateMachine.hideWidgets();

                        stateMachine.setState(tasksState);

                        setTaskOnTap();
                        break;

                    case R.id.navigation_diaries:

                        stateMachine.setState(tasksState);
                        stateMachine.hideWidgets();
                        stateMachine.setState(notesState);
                        stateMachine.hideWidgets();

                        stateMachine.setState(diaryState);

                        setDiaryOnTap();
                        break;
                }

                stateMachine.showWidgets();
                updateActionBarTitle();

                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView)menu.findItem(R.id.app_bar_search).getActionView();

        if (null != searchView) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(true);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                public boolean onQueryTextChange(String text) {

                    List<Note> notes = notesState.search(text);
                    List<Task> tasks = tasksState.search(text);
                    List<Diary> diaries = diaryState.search(text);

                    notesState.load(notes);
                    tasksState.load(tasks);
                    diaryState.load(diaries);

                    return true;
                }

                public boolean onQueryTextSubmit(String query) {

                    SearchView sv = findViewById(R.id.app_bar_search);
                    sv.setQuery("", true);
                    sv.setIconified(true);
                    sv.onActionViewCollapsed();

                    return true;
                }

            }
        );

        return true;
    }

    private void updateActionBarTitle()
    {
        getSupportActionBar().setTitle("Total " + stateMachine.getStateType() + ": " + stateMachine.getDatabaseItems().size());
    }

    private boolean isNightModeOn() {
        return AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES;
    }

    private void setNightModeOn() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
    }

    private void setNightModeOff() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.app_bar_night_mode:

                if(isNightModeOn()) {
                    setNightModeOff();
                } else {
                    setNightModeOn();
                }

                return true;

            case R.id.app_bar_clear_all:

                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Are you sure you want to delete all data?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NotepadDatabaseSingleton db = NotepadDatabaseSingleton.getInstance(MainActivity.this);
                                db.clearAllTables();
                                notesState.load(db.notesDAO().getNotes());
                                diaryState.load(db.diariesDAO().getDiaries());
                                tasksState.load(db.tasksDAO().getTasks());

                                updateActionBarTitle();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create().show();

                return true;

            case R.id.app_bar_about:
                Intent about = new Intent(this, AboutActivity.class);
                startActivity(about);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void setTaskOnTap() {
        tasksState.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent edit = new Intent(MainActivity.this, ModifyTaskActivity.class);
                TextView textView = arg1.findViewById(R.id.task_layout_text);
                edit.putExtra(TASK_EXTRA_KEY, textView.getText());
                startActivity(edit);
            }

        });
    }


    private void setNoteOnTap() {
        notesState.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent edit = new Intent(MainActivity.this, ModifyNoteActivity.class);
                TextView textView = arg1.findViewById(R.id.note_text);
                edit.putExtra(NOTE_EXTRA_KEY, textView.getText());
                startActivity(edit);
            }

        });
    }


    private void setDiaryOnTap() {
        diaryState.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent edit = new Intent(MainActivity.this, ModifyDiaryActivity.class);
                TextView textView = arg1.findViewById(R.id.diary_layout_text);
                edit.putExtra(DIARY_EXTRA_KEY, textView.getText());
                startActivity(edit);
            }

        });
    }


    private String getTitleDeleteMessageFromTextView(TextView textView) {
        String text = textView.getText().toString();
        return text.length() <= TITLE_DELETE_MESSAGE_LIMIT ? text : text.substring(0, TITLE_DELETE_MESSAGE_LIMIT) + "...";
    }

    @Override
    protected void onResume() {
        super.onResume();

        List<?> items = stateMachine.getDatabaseItems();
        stateMachine.load(items);

        updateActionBarTitle();
    }

}
