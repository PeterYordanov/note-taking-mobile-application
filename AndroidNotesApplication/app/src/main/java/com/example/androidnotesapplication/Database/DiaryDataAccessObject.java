package com.example.androidnotesapplication.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.androidnotesapplication.Models.Diary;

import java.util.List;

@Dao
public interface DiaryDataAccessObject {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDiary(Diary diary);
    @Delete
    void deleteDiary(Diary diary);
    @Update
    void updateDiary(Diary diary);
    @Query("SELECT * FROM diaries")
    List<Diary> getDiaries();
    @Query("SELECT * FROM diaries WHERE diaryId = :diaryId")
    Diary getDiaryById(int diaryId);
    @Query("DELETE FROM diaries WHERE diaryId = :diaryId")
    void deleteDiaryById(int diaryId);
    @Query("DELETE FROM diaries WHERE text = :diaryText")
    void deleteDiaryByText(String diaryText);
    @Query("SELECT * FROM diaries WHERE text = :diaryText")
    Diary getDiaryByText(String diaryText);
}