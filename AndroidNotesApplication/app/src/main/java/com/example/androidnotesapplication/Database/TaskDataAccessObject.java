package com.example.androidnotesapplication.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.androidnotesapplication.Models.Task;

import java.util.List;

@Dao
public interface TaskDataAccessObject {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTask(Task task);
    @Delete
    void deleteTask(Task task);
    @Update
    void updateTask(Task task);
    @Query("SELECT * FROM tasks")
    List<Task> getTasks();
    @Query("SELECT * FROM tasks WHERE taskId = :taskId")
    Task getTaskById(int taskId);
    @Query("DELETE FROM tasks WHERE taskId = :taskId")
    void deleteTaskById(int taskId);
    @Query("DELETE FROM tasks WHERE title = :title")
    void deleteTaskByText(String title);
    @Query("SELECT * FROM tasks WHERE title = :title")
    Task getTaskByText(String title);

}
