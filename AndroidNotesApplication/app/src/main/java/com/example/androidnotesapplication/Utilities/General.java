package com.example.androidnotesapplication.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.DatePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class General {

    public static Bitmap getBitmapFromImageUri(Uri imageUri, Context context) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return bitmap;
    }

    public static File getImageFile(Context context) {

        String imageName = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File storageDirectory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File imageFile = null;
        try {
            imageFile = File.createTempFile(imageName, ".jpg", storageDirectory);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return imageFile;
    }

    public static byte[] getByteArrayFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);

        return stream.toByteArray();
    }

    public static Bitmap getBitmapFromByteArray(byte[] image) {
        Bitmap bitmap = new BitmapFactory().decodeByteArray(image, 0, image.length);
        return bitmap;
    }

    public static long getStringDateFromDatetime(Date datetime) {
        Date date = new Date();
        try {
            DateFormat outputFormatter = new SimpleDateFormat("dd/MM/yyyy");
            date = outputFormatter.parse(outputFormatter.format(datetime));
        }
        catch (ParseException ex) {
            ex.printStackTrace();
        }

        return date.getTime();
    }

    public static Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public static String getStringDateFromLong(long datetime) {
        return new SimpleDateFormat("EEE, dd MMM yyyy hh:mm aaa", Locale.US).format(new Date(datetime));
    }


}
